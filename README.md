#NUPLAN React Native App
-----------------------
This is an application for Northeastern students to create a schedule. I am using the [SearchNEU API](https://github.com/ryanhugh/searchneu) to allow students to search through the offered courses of this current Spring 2018 semester. I am currently working on the IOS version, Android work coming soon. I am using a Redux pattern for courses.
