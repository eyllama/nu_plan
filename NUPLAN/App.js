import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Root } from './app/router';
import configureStore from './app/redux/store/ConfigureStore';

let store = configureStore();

export default class App extends Component<{}> {
  render() {
    return (
        <Provider store={store}>
            <Root/>
        </Provider>
    );
  }
}
