import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    ScrollView,
    TouchableHighlight,
    StatusBar,
    ActivityIndicator,
    Alert,
    Dimensions,
    SafeAreaView
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { styles } from '../styles';
import { SmallHeader } from '../components/headers/SmallHeader';
import Course from '../classes/Course';
import { SectionSwiper } from '../components/swipers/SectionSwiper';
import * as courseActions from '../redux/actions/CourseActions';

class CourseDetails extends Component {
    course: Course;
    constructor(props) {
        super(props);
        this.state = {
            headerShadow: false,
        };
        this._onScroll = this._onScroll.bind(this);
        this._addSection = this._addSection.bind(this);
    }

    componentWillMount = () => {
        let { course } = this.props.navigation.state.params.course;
        this.course = course;
    }

    componentDidMount = () => {
        let sec = null;
        let hasCourse = false;
        for (var i = 0; i < this.props.courses.length; i++) {
            let courseKey = this.props.courses[i].subject.toUpperCase() + this.props.courses[i].classId;
            let thisCourseKey = this.course.subject.toUpperCase() + this.course.classId;
            if (thisCourseKey === courseKey) {
                hasCourse = true;
                sec = this.props.courses[i].sectionNumber;
            }
        }
        this.setState({ userHasCourse: hasCourse });
        this.setState({ userHasSection: sec });
    }

    _onScroll = (event) => {
        let { y } = event.nativeEvent.contentOffset;
        this.setState({ headerShadow: (y > 10) });
    }

    _addSection = (sectionIndex) => {
        this.setState({ loading: true });
        let section = this.course.sections[sectionIndex];
        setTimeout(() => {
            this.props.dispatch(courseActions.addCourse(section));
            this.setState({ loading: false });
        }, 300);
        this.props.navigation.dispatch(NavigationActions.back());
    }

    render() {
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor: "#262a2d" }}>
                <View style={styles.whiteContainer}>
                    <StatusBar hidden />
                    <SmallHeader
                        title={this.course.subject + this.course.classId}
                        subTitle={this.course.name}
                        titleColor={"#fff"}
                        subColor={"#4a575b"}
                        backgroundColor={"#262a2d"}
                        shadow={this.state.headerShadow}
                        goBack={() => this.props.navigation.goBack()}
                    />
                    <ScrollView style={styles.body}
                        directionalLockEnabled
                        scrollEventThrottle={60}
                        onScroll={this._onScroll}
                    >
                        <SectionSwiper
                            sections={this.course.sections}
                            addSection={this._addSection}
                            userHasCourse={this.state.userHasCourse}
                            userHasSection={this.state.userHasSection}
                        />
                        <View style={styles.card}>
                            <Text style={[styles.text, styles.font16]}>
                                {this.course.desc}
                            </Text>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

let mapStateToProps = (state, ownProps) => {
    return {
        courses: state.courses
    }
}

export default connect(mapStateToProps)(CourseDetails);
