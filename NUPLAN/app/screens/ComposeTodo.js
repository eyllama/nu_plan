import React, { Component } from 'react';
import {
	View,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
	KeyboardAvoidingView,
	FlatList,
	ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements';
import { styles } from '../styles';
// import Item from '../classes/Item';
import Todo from '../classes/Todo';
import { TodoItem } from '../components/items/TodoItem';

class ComposeTodo extends Component {
	//item: Item = null;
	constructor(props) {
		super(props);
		this.state = {
			todos: [],
		};
		this._renderItem = this._renderItem.bind(this);
		this._addTodo = this._addTodo.bind(this);
	}

	componentDidMount = () => {
		let todos = this.props.navigation.state.params.todos;
		this.setState({ todos });
	}

	_renderItem = ({ item }) => {
		return(
			<TodoItem todo={item} />
		);
	}

	_addTodo = () => {
		let text = this.input._lastNativeText;
		if (text.length < 1) { return; }
		let todos = this.state.todos;
		let todo = new Todo(text, "Desc", new Date().getTime());
		todos.push(todo);
		this.setState({ todos });
		this.input.clear();
	}

	render() {
		return(
			<View style={styles.container}>
				<View style={styles.container}>
					<FlatList
						extraData={this.state}
						keyExtractor={(item, index) => index}
						data={this.state.todos}
						renderItem={this._renderItem}
					/>
				</View>
				<KeyboardAvoidingView behavior={"padding"}>
					<View style={styles.todoInputContainer}>
						<TextInput
							style={[_styles.text, styles.todoInput]}
							ref={(input) => this.input = input }
							placeholder={"Anything you need to do?"}
							placeholderTextColor={"#000"}
							selectionColor={"#fff"}
							keyboardAppearance={"dark"}
							autoFocus
							maxLength={200}
							multiline
						/>
						<TouchableOpacity style={[styles.center, styles.todoAddBtn]}
							activeOpacity={0.6} onPress={this._addTodo}>
							<Icon type="entypo" name="rocket" color="#fff" size={18} />
						</TouchableOpacity>
					</View>
				</KeyboardAvoidingView>
			</View>
		);
	}
}

const _styles = StyleSheet.create({
	text: {
		fontFamily: "AppleSDGothicNeo-SemiBold",
		color: "#fff",
		fontSize: 16
	},
	list: {
		flex: 1
	}
});

export default ComposeTodo;
