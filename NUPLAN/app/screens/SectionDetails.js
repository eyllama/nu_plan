import React, { Component } from 'react';
import {
    View,
    Text,
    ScrollView,
    Image,
    TouchableHighlight,
    Animated,
    Easing,
    StatusBar,
    Alert,
    ActivityIndicator,
    Dimensions,
    SafeAreaView
} from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import { styles } from '../styles';
import * as courseActions from '../redux/actions/CourseActions';
import Section from '../classes/Section';
import { SmallHeader } from '../components/headers/SmallHeader';
import { DaysInWeek } from '../components/DaysInWeek';

const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

class SectionDetails extends Component {
    section: Section = null;
    constructor(props) {
        super(props);
        this.state = {
            headerShadow: false,
            loading: false,
            days: []
        };
        this._removeSection = this._removeSection.bind(this);
        this._onScroll = this._onScroll.bind(this);
    }

    componentWillMount = () => {
        let section = this.props.navigation.state.params.section;
        this.section = section;
        //console.log(moment.utc(33300 * 1000).format('h:mm a'));
    }

    _alertRemove = () => {
        Alert.alert(
            'Wait!',
            'Are you sure you want to remove this section of ' + this.section.subject + this.section.classId + '?',
            [
                { text: 'Yes', onPress: this._removeSection },
                { text: 'No', onDismiss: () => {}, style: 'destructive' },
            ],
        );
    }

    _removeSection = () => {
        this.setState({ loading: true });
        let sectionId = this.section.subject + this.section.classId;
        setTimeout(() => {
            this.props.dispatch(courseActions.removeCourse(sectionId));
            this.setState({ loading: false });
            this.props.navigation.goBack();
        }, 300);
    }

    _onScroll = (event) => {
        let { y } = event.nativeEvent.contentOffset;
        this.setState({ headerShadow: y > 10 });
    }

    render() {
        return(
            <SafeAreaView style={{ flex: 1, backgroundColor: "#262a2d" }}>
                <View style={styles.whiteContainer}>
                    <StatusBar hidden />
                    <SmallHeader
                        title={this.section.subject + this.section.classId}
                        subTitle={this.section.name}
                        titleColor={"#fff"}
                        subColor={"#4a575b"}
                        backgroundColor={"#262a2d"}
                        shadow={false}
                        goBack={() => this.props.navigation.goBack()}
                    />
                    <DaysInWeek
                        activeDays={this.section.daysIndices}
                        backgroundColor={"#262a2d"}
                        shadow={this.state.headerShadow}

                    />
                    <ScrollView style={[styles.body, styles.padding10]}
                        directionalLockEnabled
                        scrollEventThrottle={1}
                        onScroll={this._onScroll}>
                        <Text style={[styles.text, styles.centerSelf, styles.detailTitle]}>
                            CRN {this.section.crn}
                        </Text>
                        <View style={[styles.line1, styles.centerSelf]} />
                        <View style={[styles.center, styles.bottomBorder1, styles.padding10]}>
                            {
                                this.section.daysIndices.map((day, index) => {
                                    let start = this.section.meetingTimesPretty[index].start;
                                    let end = this.section.meetingTimesPretty[index].end;
                                    return(
                                        <Text key={index} style={[styles.text,
                                            styles.centerSelf, styles.padding10]}>
                                            {weekdays[day]} at {start} - {end}
                                        </Text>
                                    )
                                })
                            }
                        </View>
                        <View style={[styles.center, styles.bottomBorder1, styles.padding15]}>
                            <View style={[styles.row, styles.center]}>
                                <Image style={[styles.detailIcon, styles.marginVertical10,
                                    styles.marginHorizontal10]}
                                    source={require('../images/books_icon_1.png')}
                                />
                                <Image style={[styles.detailIcon, styles.marginVertical10,
                                    styles.marginHorizontal10]}
                                    source={require('../images/student_icon_1.png')}
                                />
                                <Image style={[styles.detailIcon, styles.marginVertical10,
                                    styles.marginHorizontal10]}
                                    source={require('../images/seats_icon_1.png')}
                                />
                            </View>
                            <Text style={[styles.text, styles.detailTitle]}>
                                Section Number
                            </Text>
                            <Text style={[styles.text]}>
                                {this.section.sectionNumber}
                            </Text>
                        </View>
                    </ScrollView>
                    <TouchableHighlight style={[styles.center, styles.removeBtn]}
                        underlayColor={"#bc3225"}
                        onPress={() => this._alertRemove()}>
                        {
                            (() => {
                                if (this.state.loading) {
                                    return <ActivityIndicator color={"#fff"} size={"small"} />
                                } else {
                                    return <Text style={[styles.text, styles.whiteText]}>Remove</Text>
                                }
                            })()
                        }
                    </TouchableHighlight>
                </View>
            </SafeAreaView>
        );
    }
}

export default connect()(SectionDetails);
