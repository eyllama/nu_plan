import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    FlatList,
    Animated,
    Easing,
    Dimensions,
    AsyncStorage,
    ActivityIndicator
} from 'react-native';
import moment from 'moment';
import { styles } from '../../styles';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { DefaultHeader } from '../../components/headers/DefaultHeader';
import { ScheduleItem } from '../../components/items/ScheduleItem';
import ScheduleDaysFilter from '../../components/ScheduleDaysFilter';

const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const today = new Date();
const todayIndex = today.getDay();

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dayFilter: today.getDay(),
            isDayContainerOpen: false
        };
        this._renderItem = this._renderItem.bind(this);
        this._goToSection = this._goToSection.bind(this);
        this._toggleDayContainer = this._toggleDayContainer.bind(this);
        this.filterDaysTranslateValue = new Animated.Value(0);
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.courses !== this.props.courses) {
            let courses = nextProps.courses.filter(c => c.daysIndices.includes('' + this.state.dayFilter));
            //let courses = nextProps.courses;
            this.setState({ courses });
        }
    }

    _goToSection = (section) => {
        this.props.navigation.navigate("SectionDetails", { section });
    }

    _renderItem = ({ item }) => {
        return(
            <ScheduleItem section={item} goToSection={this._goToSection} />
        );
    }

    _updateCoursesByActiveDay = (dayIndex) => {
        this.setState({ isDayContainerOpen: !this.state.isDayContainerOpen });
        setTimeout(() => {
            this.setState({ dayFilter: dayIndex });
            let courses;
            if (dayIndex === "View All") {
                courses = this.props.courses;
            } else {
                courses = this.props.courses.filter(c => c.daysIndices.includes('' + dayIndex));
            }
            this.setState({ courses });
        }, 150);
    }

    _toggleDayContainer = () => {
        let s = this.state.isDayContainerOpen ? 1 : 0;
        let e = this.state.isDayContainerOpen ? 0 : 1;

        this.filterDaysTranslateValue.setValue(s);
        Animated.timing(
            this.filterDaysTranslateValue,
            {
                toValue: e,
                duration: 200,
                easing: Easing.ease
            }
        ).start();
        this.setState({ isDayContainerOpen: !this.state.isDayContainerOpen });
    }

    render() {
        return(
            <View style={styles.container}>
                <DefaultHeader
                    title={"Schedule"}
                    color={"#fff"}
                    backgroundColor={"#db4234"}
                    nextBtn={false}
                    iconColor={"#fff"}
                    goToSearch={() => this.props.navigation.navigate("Search")}
                />
                <View style={[styles.scheduleDayFilter, styles.row]}>
                    <Text style={[styles.text, styles.flex1, { color: "#f9a29a" }]}>
                        {this.state.dayFilter === "View All" ? "All" : days[this.state.dayFilter]}
                    </Text>
                    <TouchableOpacity style={[styles.toggleFilterBtn, styles.center]}
                        onPress={this._toggleDayContainer}>
                        <Icon type="entypo" name="dots-three-horizontal"
                            color={this.state.isDayContainerOpen ? "#fff" : "#f9a29a" }
                            size={20} />
                    </TouchableOpacity>
                </View>
                <ScheduleDaysFilter selectedDay={this.state.dayFilter}
                    shouldShow={this.state.isDayContainerOpen}
                    updateCoursesByActiveDay={this._updateCoursesByActiveDay}
                />
                <AnimatedFlatList
                    keyExtractor={(item, index) => index}
                    data={this.state.courses}
                    renderItem={this._renderItem}
                />
            </View>
        );
    }
}

let mapStateToProps = (state, ownProps) => {
    return {
        courses: state.courses
    }
}

export default connect(mapStateToProps)(Schedule);
