import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
    ScrollView,
    TouchableHighlight,
    Image,
    TextInput,
    KeyboardAvoidingView,
    ActivityIndicator,
    Animated,
    Dimensions
} from 'react-native';
import axios from 'axios';
import { styles } from '../../styles';
import { DefaultHeader } from '../../components/headers/DefaultHeader';
import { SearchItem } from '../../components/items/SearchItem';

const { width, height } = Dimensions.get('window');
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
const TOP_MAX = 190;
const TOP_MIN = 45;
const TOP_SCROLL_DIST = TOP_MAX - TOP_MIN;

const filters = [
    { title: "Course", uri: "https://3pqucg35uzjnjg0ji38maplh-wpengine.netdna-ssl.com/wp-content/uploads/2016/03/flat-design.jpg" },
    { title: "Subject", uri: "https://designcontest-com-designcontest.netdna-ssl.com/blog/wp-content/uploads/2013/07/flat-design-office-desk-02-preview-o.jpg" },
    { title: "Professor", uri: "https://design4users.com/wp-content/uploads/2016/12/star-wars-rogue-one_tubik_studio_illustration.png" }
];

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: false,
            filter: "course"
        };
        this.scrollY = new Animated.Value(0);
        this._renderItem = this._renderItem.bind(this);
        this._onFocus = this._onFocus.bind(this);
        this._onFilterScroll = this._onFilterScroll.bind(this);
        this._onChange = this._onChange.bind(this);
    }

    _renderItem = ({ item }) => {
        return(
            <SearchItem item={item} goToCourse={this._goToCourse} />
        );
    }

    _onFocus = () => {
        this.refs.flatlist._component.scrollToOffset({ x: 0, y: 0, animated: true });
    }

    _onFilterScroll = (event) => {
        this.setState({ data: [] });
        this.refs.input._component.clear();
        // this.refs._input._component.blur(); // TODO maybe remove this feature
        let { x } = event.nativeEvent.contentOffset;
        if (x === 0) {
            this.setState({ filter: "course" });
        } else if (x === width) {
            this.setState({ filter: "subject" });
        } else if(x === (2 * width)) {
            this.setState({ filter: "professor" });
        }
    }

    getMaxForType = () => {
        let type = this.state.filter;
        let result;
        switch (type) {
            case "course":
                result = 4;
                break;
            case "subject":
                result = 150;
                break;
            case "professor":
                result = 5;
            default:
                break;
        }
        return result;
    }

    _goToCourse = (course) => {
        this.props.navigation.navigate("Details", { course });
    }

    _search = () => {
        this.setState({ loading: true });
        let query = this.refs.input._component._lastNativeText;
        let max = this.getMaxForType();
        let url = "https://searchneu.com/search?query="
            + query + "&termId=201830&minIndex=0&maxIndex=" + max;
        axios.get(url)
            .then((response) => {
                this.setState({
                    error: false,
                    loading: false,
                    data: response.data
                });
                this.refs.input._component.blur();
            }).catch((err) => {
                this.setState({
                    error: true,
                    loading: false,
                    data: []
                });
            });
    }

    _onChange = (event) => {
        let text = event.nativeEvent.text;
        if (text.length === 0) {
            this.setState({ data: [] });
        }
    }

    render() {
        const topTranslate = this.scrollY.interpolate({
            inputRange: [0, TOP_SCROLL_DIST],
            outputRange: [0, -TOP_SCROLL_DIST],
            extrapolate: "clamp"
        });
        const imageTranslate = this.scrollY.interpolate({
            inputRange: [0, TOP_SCROLL_DIST],
            outputRange: [0, 100],
            extrapolate: "clamp"
        });
        const titleTranslate = this.scrollY.interpolate({
            inputRange: [0, TOP_SCROLL_DIST],
            outputRange: [0, 75],
            extrapolate: "clamp"
        });
        const inputTranslate = this.scrollY.interpolate({
            inputRange: [0, TOP_SCROLL_DIST],
            outputRange: [0, 5],
            extrapolate: "clamp"
        });
        const titleOpacity = this.scrollY.interpolate({
            inputRange: [0, TOP_SCROLL_DIST / 2, TOP_SCROLL_DIST],
            outputRange: [1, 0.6, 0.3],
            extrapolate: "clamp"
        });
        const topShadowOpacity = this.scrollY.interpolate({
            inputRange: [0, 10],
            outputRange: [0, 1],
            extrapolate: "clamp"
        });
        return(
            <View style={styles.container}>
                <DefaultHeader
                    title={"Search"}
                    color={"#4a575b"}
                    backgroundColor={"#fff"}
                />
                <Animated.View style={[_styles.top, this.state.data.length > 1 ? styles.shadow1 : null,
                    { transform: [{ translateY: topTranslate }] }]}>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled
                        scrollEventThrottle={60}
                        onScroll={this._onFilterScroll}
                    >
                        {
                            filters.map((filter, index) => {
                                return(
                                    <Filter key={index} filter={filter}
                                        imageTranslate={imageTranslate}
                                        titleTranslate={titleTranslate}/>
                                )
                            })
                        }
                    </ScrollView>
                    <Animated.View style={[_styles.searchInputContainer,
                        { opacity: titleOpacity }]}>
                        <AnimatedTextInput
                            style={[styles.text, _styles.searchInput,
                            { transform: [{ translateX: inputTranslate }] }]}
                            ref={"input"}
                            placeholder={"Search by " + this.state.filter}
                            placeholderTextColor={"#ccc"}
                            selectionColor={"#db4234"}
                            selectTextOnFocus
                            onFocus={this._onFocus}
                            keyboardAppearance={"dark"}
                            clearButtonMode={"while-editing"}
                            onChange={this._onChange}
                            underlineColorAndroid={"transparent"}
                            returnKeyLabel={"Search"}
                            onSubmitEditing={this._search}
                        />
                    </Animated.View>
                </Animated.View>
                <AnimatedFlatList
                    style={[_styles.flatlist]}
                    ref={"flatlist"}
                    extraData={this.state.data}
                    keyExtractor={(item, index) => index}
                    data={this.state.data}
                    renderItem={this._renderItem}
                    scrollEnabled={this.state.data.length > 0}
                    scrollEventThrottle={1}
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.scrollY } } }],
                        { useNativeDriver: true }
                    )}
                    contentContainerStyle={{ paddingTop: TOP_MAX }}
                />
                <KeyboardAvoidingView behavior={"padding"}
                    keyboardVerticalOffset={40}>
                    <TouchableHighlight style={[styles.center, styles.searchBtn]}
                        underlayColor={"#bc3225"}
                        onPress={this._search}>
                        {
                            (() => {
                                if (this.state.loading) {
                                    return <ActivityIndicator color={"#fff"} size={"small"} />
                                } else {
                                    return <Text style={[styles.text, { color: "#fff" }]}>Search</Text>
                                }
                            })()
                        }
                    </TouchableHighlight>
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const Filter = (props) => {
    return(
        <View style={[styles.filter, styles.center]}>
            <Animated.Image style={[styles.filterImg,
                { transform: [{ translateY: props.imageTranslate }] } ]}
                source={{ uri: props.filter.uri }} /*blurRadius={props.blurRadius}*/ resizeMode={"cover"} />
            <Animated.Text style={[styles.text, styles.filterTitle,
                { transform: [{ translateY: props.titleTranslate }] } ]}>
                {props.filter.title}
            </Animated.Text>
        </View>
    );
}

const _styles = StyleSheet.create({
    top: {
        position: "absolute",
        top: 80,
        left: 0,
        right: 0,
        height: TOP_MAX,
        backgroundColor: "#fff",
        // overflow: "hidden",
        zIndex: 5
    },
    flatlist: {
        flex: 1,
        bottom: 0
    },
    item: {

    },
    searchInputContainer: {
        height: 45,
        backgroundColor: "#fff",
        justifyContent: "center"
    },
    searchInput: {
        height: 45,
        fontSize: 18,
        color: "#4a575b",
        paddingHorizontal: 10
    },
});

export default Search;
