import * as types from '../actions/ActionTypes';

const initialState = [];

const itemReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_ITEM:
            return [
                ...state,
                Object.assign({}, action.item)
            ];
        case types.REMOVE_ITEM:
            let schedule = state.filter(item => {
                let name = item.section.subject.toUpperCase() + item.section.classId;
                if (name === action.itemId) {
                    return false;
                }
                return true;
            });
            return schedule;
        case types.REMOVE_ALL:
            return initialState;
        default:
            return state;
    }
}

export default itemReducer;
