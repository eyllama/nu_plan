import * as types from '../actions/ActionTypes';

const initialState = [];

const courseReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_COURSE:
            return [
                ...state,
                Object.assign({}, action.section)
            ];
        case types.REMOVE_COURSE:
            let schedule = state.filter(section => {
                let name = section.subject.toUpperCase() + section.classId;
                if (name === action.sectionId) {
                    return false;
                }
                return true;
            });
            return schedule;
        case types.REMOVE_ALL:
            return initialState;
        default:
            return state;
    }
}

export default courseReducer;
