import * as types from './ActionTypes';

export const addCourse = (section) => {
    return {
        type: types.ADD_COURSE,
        section
    }
}

export const removeCourse = (sectionId) => {
    return {
        type: types.REMOVE_COURSE,
        sectionId
    }
}

export const removeAll = () => {
    return {
        type: types.REMOVE_ALL
    }
}
