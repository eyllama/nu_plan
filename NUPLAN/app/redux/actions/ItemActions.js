import * as types from './ActionTypes';

export const addItem = (item) => {
    return {
        type: types.ADD_ITEM,
        item
    }
}

export const removeItem = (itemId) => {
    return {
        type: types.REMOVE_ITEM,
        itemId
    }
}

export const removeAll = () => {
    return {
        type: types.REMOVE_ALL
    }
}
