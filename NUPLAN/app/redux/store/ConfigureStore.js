import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { AsyncStorage } from 'react-native';
import rootReducer from '../reducers/RootReducer';

let defaultState = {};

const configureStore = (initialState = defaultState) => {
    let store = createStore(rootReducer, initialState, compose(
        applyMiddleware(),
        autoRehydrate()
    ));
    persistStore(store, { storage: AsyncStorage });
    return store;
}

export default configureStore;
