import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableHighlight,
    ScrollView,
    Alert,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';
import { Icon } from 'react-native-elements';
const { width, height } = Dimensions.get('window');

export const SectionSwiper = (props) => {
    const { userHasCourse, userHasSection } = props;
    const _handleAddBtnPress = (index) => {
        if (userHasCourse) {
            Alert.alert(
                'Are you sure?',
                "You already have a section of this course in your schedule. Pressing 'Continue' will replace it with this section",
                [
                    { text: 'Continue', onPress: () => props.addSection(index)},
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                ],
                { cancelable: true }
            );
        } else {
            props.addSection(index);
        }
    }
    return(
        <View style={_styles.container}>
            <ScrollView horizontal
                directionalLockEnabled
                decelerationRate={0}
                snapToInterval={width - 50}
                snapToAlignment={"center"}
                showsHorizontalScrollIndicator={false}>
                {
                    props.sections.map((section, index) => {
                        return(
                            <View key={index} style={[_styles.sectionContainer]}>
                                <View style={styles.flex1, { height: 210 }}>
                                    <Text style={[styles.text, styles.font16]}>
                                        Section {section.sectionNumber}
                                    </Text>
                                </View>
                                {
                                    (() => {
                                        if (props.userHasSection === section.sectionNumber) {
                                            return(
                                                <View style={[_styles.hasSectionIndicator, styles.center]}>
                                                    <Icon type="entypo" name="check" color="#fff" size={12} />
                                                </View>
                                            )
                                        }
                                    })()
                                }
                                <TouchableHighlight style={[_styles.addSectionBtn, styles.center,
                                    { backgroundColor: props.userHasClass ? "#efefef" : "#db4234" }]}
                                    onPress={() => _handleAddBtnPress(index)}>
                                    <Text style={[styles.text, styles.whiteText]}>
                                        Add {section.crn}
                                    </Text>
                                </TouchableHighlight>
                            </View>
                        )
                    })
                }
            </ScrollView>
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 260,
        paddingTop: 10,
        backgroundColor: "#efefef"
    },
    sectionContainer: {
        width: width - 60,
        marginHorizontal: 5,
        height: 240,
        backgroundColor: "#fff"
    },
    addSectionBtn: {
        position: "absolute",
        right: 0,
        bottom: 0,
        width: 100,
        height: 30,
        margin: 10,
    },
    hasSectionIndicator: {
        position: "absolute",
        left: 0,
        bottom: 0,
        width: 20,
        height: 20,
        margin: 10,
        backgroundColor: "#45e098",
        borderRadius: 10
    }
});
