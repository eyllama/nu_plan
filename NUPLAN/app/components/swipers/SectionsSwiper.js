import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableHighlight,
    Animated,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';

const { width, height } = Dimensions.get('window');

export const SectionsSwiper = (props) => {
    const _emitAddSection = (index) => {
        props.addSection(index);
    }
    return(
        <View style={_styles.container}>
            <Animated.ScrollView style={[_styles.container, styles.shadow1]}
                directionalLockEnabled
                horizontal
                decelerationRate={0}
                snapToInterval={width - 50}
                snapToAlignment={"center"}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{ padding: 5 }}
            >
                {
                    props.sections.map((section, index) => {
                        return(
                            <View key={index} style={[_styles.section, styles.shadow1]}>
                                <Text style={[styles.text, styles.font18]}>Section {section.sectionNumber}</Text>
                                {/*<TouchableHighlight style={[_styles.addBtn, styles.center]}
                                    underlayColor={"#000"}
                                    onPress={() => _emitAddSection(index)}
                                >
                                    <Text style={[styles.text, styles.whiteText]}>
                                        Add {section.crn}
                                    </Text>
                                </TouchableHighlight>*/}
                            </View>
                        )
                    })
                }
            </Animated.ScrollView>
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 300,
        marginTop: 5
    },
    section: {
        width: width - 60,
        height: 300,
        marginHorizontal: 5,
        backgroundColor: "#fff"
    },
    addBtn: {
        width: width - 80,
        height: 40,
        borderRadius: 1,
        backgroundColor: "#3f3d4f"
    },
});
