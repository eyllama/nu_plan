import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableHighlight,
    Image,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';
import Course from '../../classes/Course';
import Section from '../../classes/Section';
import Meeting from '../../classes/Meeting';

export const SearchItem = (props) => {

    const _emitGoToCourse = () => {
        let { name, subject, classId, desc, maxCredits, minCredits } = props.item.class;
        let courseSections = [];
        for (let i = 0; i < props.item.sections.length; i++) {
            let s = props.item.sections[i];
            let meetings = s.meetings;
            let meetingArray = [];
            for (let j = 0; j < meetings.length; j++) {
                let m = meetings[j];
                meetingArray.push(new Meeting(m.startDate, m.endDate, m.profs, m.where, m.times));
            }
            courseSections.push(new Section(i + 1, name, subject, classId, s.crn, s.honors, s.seatsCapacity, s.seatsRemaining, meetingArray));
        }
        let course = new Course(name, subject, classId, desc, courseSections, maxCredits, minCredits);
        props.goToCourse({ course });
    }

    return(
        <TouchableHighlight style={_styles.container}
            underlayColor={"#efefef"}
            onPress={() => _emitGoToCourse()}>
            <View style={_styles.innerContainer}>
                <Text style={[styles.text, _styles.title]}>
                    {props.item.class.name}
                </Text>
                <Text style={[styles.subTitle, { marginBottom: 10 }]}>
                    {props.item.class.subject + props.item.class.classId}
                </Text>
                <View style={[styles.line1]} />
                <Text style={[styles.text, styles.desc]}
                    numberOfLines={8}>
                    {props.item.class.desc}
                </Text>
            </View>
        </TouchableHighlight>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 255,
        marginTop: 10,
        padding: 15,
        backgroundColor: "#fff"
    },
    innerContainer: {
        flex: 1
    },
    title: {
        fontSize: 18,
        marginVertical: 10
    }
});
