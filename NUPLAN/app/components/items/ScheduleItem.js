import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableHighlight,
    Image,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';

export const ScheduleItem = (props) => {

    const _emitGoToSection = () => {
        props.goToSection(props.section);
    }

    return(
        <TouchableHighlight style={_styles.container}
            underlayColor={"#efefef"}
            onPress={() => _emitGoToSection()}>
            <View style={_styles.innerContainer}>
                <Text style={[styles.text, _styles.title]}>
                    {props.section.name}
                </Text>
                <Text style={[styles.text, styles.subText]}>
                    {props.section.subject + props.section.classId}
                </Text>
            </View>
        </TouchableHighlight>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 100,
        padding: 15,
        marginTop: 10,
        backgroundColor: "#fff"
    },
    innerContainer: {
        flex: 1
    },
    title: {
        fontSize: 18,
        marginBottom: 5
    }
});
