import React from 'react';
import {
    View,
    StyleSheet,
    Text
} from 'react-native';
import { Icon } from 'react-native-elements';
import { styles } from '../../styles';

export const TodoItem = (props) => {
    const _emitRemoveTodo = () => {

    }
    return(
        <View style={_styles.container}>
            <Text style={[styles.textMd]}
                numberOfLines={6}
                ellipsizeMode={"tail"}>
                {props.todo.title}
            </Text>
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        padding: 15,
        backgroundColor: "#fff"
    },

});
