import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    Animated,
    Easing
} from 'react-native';
import { connect } from 'react-redux';
import { styles } from '../styles';
import { Icon } from 'react-native-elements';

const weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const today = new Date();
const todayIndex = '' + today.getDay();

class ScheduleDaysFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.filterDaysTopValue = new Animated.Value(0);
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.shouldShow != this.props.shouldShow) {
            if (nextProps.shouldShow) {
                this._animateTop(0, 1);
            } else {
                this._animateTop(1, 0);
            }
        }
    }

    _animateTop = (s, e) => {
        this.filterDaysTopValue.setValue(s);
        Animated.timing(
            this.filterDaysTopValue,
            {
                toValue: e,
                duration: 100,
                easing: Easing.ease
            }
        ).start();
    }

    _dayOnClick = (day) => {
        let index = weekdays.indexOf(day);
        this.props.updateCoursesByActiveDay(index);
    }

    _viewAll = () => {
        this.props.updateCoursesByActiveDay("View All");
    }

    render() {
        const filterDaysTop = this.filterDaysTopValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-5, 125]
        });
        return(
            <Animated.View style={[styles.dayFilterContainer, styles.center, styles.shadow1,
                { top: filterDaysTop }]}>
                <View style={[styles.row]}>
                    <TouchableOpacity style={[styles.flex1, styles.center]}
                        onPress={() => this._dayOnClick("Monday")}>
                        <Image style={[styles.dayFilterImg]}
                            source={this.props.selectedDay === 1
                                ? require('../images/monday_icon1.png')
                                : require('../images/monday_icon2.png')}
                            resizeMode={"cover"} />
                        <Text style={[styles.text, styles.dayFilterTitle]}>Monday</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.flex1, styles.center]}
                        onPress={() => this._dayOnClick("Tuesday")}>
                        <Image style={[styles.dayFilterImg]}
                            source={this.props.selectedDay === 2
                                ? require('../images/tues_icon1.png')
                                : require('../images/tues_icon2.png')}
                            resizeMode={"cover"} />
                        <Text style={[styles.text, styles.dayFilterTitle]}>Tuesday</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.flex1, styles.center]}
                        onPress={() => this._dayOnClick("Wednesday")}>
                        <Image style={[styles.dayFilterImg]}
                            source={this.props.selectedDay === 3
                                ? require('../images/wed_icon1.png')
                                : require('../images/wed_icon2.png')}
                            resizeMode={"cover"} />
                        <Text style={[styles.text, styles.dayFilterTitle]}>Wednesday</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.flex1, styles.center]}
                        onPress={() => this._dayOnClick("Thursday")}>
                        <Image style={[styles.dayFilterImg]}
                            source={this.props.selectedDay === 4
                                ? require('../images/thurs_icon1.png')
                                : require('../images/thurs_icon2.png')}
                            resizeMode={"cover"} />
                        <Text style={[styles.text, styles.dayFilterTitle]}>Thursday</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.flex1, styles.center]}
                        onPress={() => this._dayOnClick("Friday")}>
                        <Image style={[styles.dayFilterImg]}
                            source={this.props.selectedDay === 5
                                ? require('../images/friday_icon1.png')
                                : require('../images/friday_icon2.png')}
                            resizeMode={"cover"} />
                        <Text style={[styles.text, styles.dayFilterTitle]}>Friday</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={this._viewAll}>
                    <Text style={[styles.text, styles.subText, { marginTop: 10 }]}>
                        View All
                    </Text>
                </TouchableOpacity>
            </Animated.View>
        );
    }
}


export default ScheduleDaysFilter;
