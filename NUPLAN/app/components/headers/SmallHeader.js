import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';
import { Icon } from 'react-native-elements';

export const SmallHeader = (props) => {
    const _goBack = () => {
        props.goBack();
    }
    return(
        <View style={[_styles.container, props.shadow ? styles.shadow2 : null,
            { backgroundColor: props.backgroundColor }]}>
            <TouchableOpacity style={[styles.center, styles.closeBtn]}
                activeOpacity={0.6} onPress={() => _goBack()}>
                <Icon type="feather" name="x" color={props.subColor} size={24} />
            </TouchableOpacity>
            <View style={[styles.center, styles.titleContainer]}>
                <Text style={[styles.text, _styles.title, { color: props.titleColor }]}>
                    {props.title}
                </Text>
                <Text style={[styles.text, styles.subTitle, { color: props.subColor }]}
                    numberOfLines={1}
                    ellipsizeMode={"tail"}>
                    {props.subTitle}
                </Text>
            </View>
            <TouchableOpacity style={styles.closeBtn}
                activeOpacity={0.6} onPress={() => _goBack()}>
                <Text></Text>
            </TouchableOpacity>
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 60,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: "#fff",
        zIndex: 10
    },
    title: {
        fontSize: 18,
    }
});
