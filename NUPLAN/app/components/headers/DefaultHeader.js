import React from 'react';
import {
    View,
    StyleSheet,
    TouchableHighlight,
    TouchableOpacity,
    Text,
    Animated,
    Easing,
    Dimensions
} from 'react-native';
import { styles } from '../../styles';
import { Icon } from 'react-native-elements';

export const DefaultHeader = (props) => {
    const _goBack = () => {
        props.goBack();
    }
    const _goToSearch = () => {
        props.goToSearch();
    }
    return(
        <View style={[_styles.container,
            { backgroundColor: props.backgroundColor } ]}>
            {
                (() => {
                    if (props.backBtn) {
                        return (
                            <TouchableOpacity style={[styles.headerBtn, styles.center]}
                                activeOpacity={0.6} onPress={() => _goBack()}>
                                <Icon type="feather" name="arrow-left" color="#4a575b" size={24} />
                            </TouchableOpacity>
                        );
                    }
                })()
            }
            <View style={_styles.titleContainer}>
                <Text style={[styles.text, _styles.title,
                    { color: props.color }]}>
                    {props.title}
                </Text>
            </View>
            {
                (() => {
                    if (props.nextBtn) {
                        return (
                            <TouchableOpacity style={[styles.headerBtn, styles.center]}
                                activeOpacity={0.6} onPress={() => _goToSearch()}>
                                <Icon type="feather" name="search" color={props.iconColor} size={18} />
                            </TouchableOpacity>
                        );
                    }
                })()
            }
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        height: 80,
        flexDirection: "row",
        alignItems: "flex-end",
        paddingHorizontal: 15,
        paddingBottom: 10,
        zIndex: 10
    },
    titleContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "flex-start"
    },
    title: {
        fontSize: 22
    }
});
