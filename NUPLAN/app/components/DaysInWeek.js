import React from 'react';
import {
    View,
    StyleSheet,
    Text
} from 'react-native';
import { styles } from '../styles';

const todayIndex = '' + (new Date().getDay());
const days = [
    { index: "1", value: "M" },
    { index: "2", value: "T" },
    { index: "3", value: "W" },
    { index: "4", value: "R" },
    { index: "5", value: "F" },
];

export const DaysInWeek = (props) => {

    return(
        <View style={[_styles.container, props.shadow ? styles.shadow2 : null,
            { backgroundColor: props.backgroundColor }]}>
            {
                days.map((day, index) => {
                    return(
                        <View key={index} style={[_styles.dayContainer, styles.center]}>
                            <View style={[_styles.day, styles.center,
                                { backgroundColor: props.activeDays.includes(day.index) ? "#4a575b" : props.backgroundColor },
                                day.index === todayIndex ? _styles.active : null]}>
                                <Text style={[styles.text, styles.whiteText]}>
                                    {day.value}
                                </Text>
                            </View>
                        </View>
                    )
                })
            }
        </View>
    );
}

const _styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        height: 50,
        alignItems: "center",
        zIndex: 4
    },
    dayContainer: {
        flex: 1,
        height: 50
    },
    day: {
        width: 34,
        height: 34,
        borderRadius: 3
    },
    active: {
        borderWidth: 3,
        borderColor: "transparent",
        borderBottomColor: "#db4234",
        //borderRadius: 0
    }
});
