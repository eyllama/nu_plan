import Meeting from './Meeting';
import Course from './Course';
import moment from 'moment';

class Section { // extends Course {
    sectionNumber: Number;
    name: String;
    subject: String;
    classId: String;
    crn: Number;
    honors: Boolean;
    seatsTotal: Number;
    seatsTaken: Number;
    meetings: Meeting;

    daysIndices: Array<Integer>;
    meetingTimesPretty: Array<Object>;

    constructor(sectionNumber, name, subject, classId, crn, honors, seatsTotal, seatsTaken, meetings) {
        // super(name, subject, classId);
        this.sectionNumber = sectionNumber;
        this.name = name;
        this.subject = subject;
        this.classId = classId;
        this.crn = crn;
        this.honors = honors;
        this.seatsTotal = seatsTotal;
        this.seatsTaken = seatsTaken;
        this.meetings = meetings;

        this.daysIndices = Object.keys(this.meetings[0].times);
        this.meetingTimesPretty = [];

        this.daysIndices.forEach((dayIndex) => {
            let s = this.meetings[0].getStartOnDay(dayIndex);
            let e = this.meetings[0].getEndOnDay(dayIndex);
            this.meetingTimesPretty.push({ start: s, end: e });
        });

    }

    toString(): String {
        return this.subject.toUpperCase() + this.classId;
    }

    compareTo(other: Section): Number {

        let thisDiff = moment().diff(moment().hour(h1).minute(m1).second(0), 'hours');
        let thatDiff = moment().diff(moment().hour(h2).minute(m2).second(0), 'hours');

        // let d = new Date();
        // let today = d.getDay();
        // let hour = d.getHours();
        //
        // if (this.meetings[0].times.includes(today)
        //     && !other.meetings[0].times.includes(today)) {
        //         return -1;
        //     } else if (!this.meetings[0].times.includes(today)
        //         && other.meetings[0].times.includes(today)) {
        //         return 1;
        //     } else {
        //         let
        //     }
    }

}

export default Section;
