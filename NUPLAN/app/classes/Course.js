import Section from './Section';

class Course {
    name: String;
    subject: String;
    classId: String;
    desc: String;
    sections: Array<Section>;
    maxCredits: Number;
    minCredits: Number;

    constructor(name, subject, classId, desc, sections,
        maxCredits, minCredits) {
        this.name = name;
        this.subject = subject;
        this.classId = classId;
        this.desc = desc;
        this.sections = sections;
        this.maxCredits = maxCredits;
        this.minCredits = minCredits;
    }

    getShortName(): String {
        return this.subject.toUpperCase() + this.classId;
    }

    compareTo(other: Course) {
        if (this.classId > other.classId) {
            return 1;
        } else if (this.classId < other.classId) {
            return -1;
        } else {
            return 0;
        }
    }

    equals(other: Object): Boolean {
        if (typeof other !== "Course") {
            return false;
        }
        let that: Course = other;
        return (that.name === this.name &&
            that.subject === this.subject &&
            that.classId === this.classId);
    }

    hashCode(): Number {
        return this.classId;
    }
}

export default Course;
