import moment from 'moment';

class Meeting {
    startDate: Number;
    endDate: Number;
    profs: Array<String>;
    where: String;
    times: Array<Object>;

    constructor(startDate, endDate, profs, where, times) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.profs = profs;
        this.where = where;
        this.times = times;
    }

    getStartOnDay(dayIndex): Object {
        let s = this.times['' + dayIndex][0].start;
        let start = moment.utc(s * 1000).format('h:mm');
        //let result = { h: start.hour(), m: start.minute() };
        return start;
    }

    getEndOnDay(dayIndex): Object {
        let e = this.times['' + dayIndex][0].end;
        let end = moment.utc(e * 1000).format('h:mm A');
        //let result = { h: end.hour(), m: end.minute() };
        // TODO add ampm value to result
        return end;
    }


}

export default Meeting;
