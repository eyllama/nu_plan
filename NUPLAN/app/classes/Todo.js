
// represents a todo item for a course (section)

class Todo {
	title: String;
	description: String;
	deadline: Date;

	constructor(title, description, deadline) {
		this.title = title;
		this.description = description;
		this.deadline = deadline;
	}

	toString(): String {
		return this.description;
	}

}

export default Todo;