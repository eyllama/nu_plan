import Section from './Section';
import Todo from './Todo';

/*
* Represents a schedule item.
* Includes both a course section with all its information, as well
* as any todos for that section (for example a HW assignment reminder)
*/

class Item {
	section: Section;
	todos: Array<Todo>;

	constructor(section, todos) {
		this.section = section;
		this.todos = todos;
	}

	addTodo(todo: Todo): void {
		this.todos.push(todo);
	}

}

export default Item;