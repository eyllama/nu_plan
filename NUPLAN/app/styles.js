import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export const styles = StyleSheet.create({
    // generic
    container: {
        flex: 1,
        backgroundColor: "#f2f2f2"
    },
    whiteContainer: {
        flex: 1,
        backgroundColor: "#fff"
    },
    flex1: {
        flex: 1,
    },
    body: {

    },
    padding10: {
        padding: 10
    },
    padding15: {
        padding: 15,
    },
    card: {
        marginTop: 10,
        padding: 10,
        backgroundColor: "#fff"
    },
    text: {
        fontFamily: "AppleSDGothicNeo-SemiBold",
        color: "#4a575b",
        fontSize: 14, //414a59
    },
    bold: {
        fontWeight: "bold"
    },
    font16: {
        fontSize: 16
    },
    font18: {
        fontSize: 18
    },
    subText: {
        fontSize: 12,
        color: "#ccc"
    },
    subTitle: {
        fontSize: 14,
        color: "#ccc"
    },
    whiteText: {
        color: "#fff"
    },
    detailTitle: {
        fontSize: 18,
        marginVertical: 10
    },
    detailIcon: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: "#efefef"
    },
    headerBtn: {
        width: 30,
        height: 30,
        backgroundColor: "transparent"
    },
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    centerSelf: {
        alignSelf: "center"
    },
    row: {
        flexDirection: "row",
        alignItems: "center"
    },
    shadow1: {
        shadowOpacity: 0.2,
        shadowColor: "rgba(0,0,0,0.5)",
        shadowRadius: 6,
        shadowOffset: { width: 0, height: 3 }
    },
    shadow2: {
        shadowOpacity: 0.5,
        shadowColor: "rgba(0,0,0,0.7)",
        shadowRadius: 8,
        shadowOffset: { width: 0, height: 5 }
    },
    bottomBorder1: {
        borderBottomWidth: 1,
        borderBottomColor: "#efefef"
    },
    line1: {
        height: 3,
        width: 80,
        backgroundColor: "#efefef",
        marginBottom: 10
    },
    fullLine: {
        width,
        height: 1,
        backgroundColor: "#efefef",
        marginBottom: 10
    },
    marginVertical10: {
        marginVertical: 10
    },
    marginHorizontal10: {
        marginHorizontal: 10
    },
    paddingHorizontal15: {
        paddingHorizontal: 15
    },
    // header
    titleContainer: {
        width: width - 120,
        height: 60
    },
    closeBtn: {
        width: 60,
        height: 60,
        backgroundColor: "transparent"
    },
    // day filter container
    scheduleDayFilter: {
        height: 45,
        backgroundColor: "#db4234",
        paddingLeft: 15,
        zIndex: 2
    },
    dayFilterContainer: {
        position: "absolute",
        left: 0,
        right: 0,
        height: 130,
        backgroundColor: "#fff",
        zIndex: 1
    },
    dayFilterImg: {
        width: 60,
        height: 60,
        borderRadius: 30,
        backgroundColor: "transparent",
        marginBottom: 5
    },
    dayFilterTitle: {
        fontSize: 10
    },
    toggleFilterBtn: {
        width: 45,
        height: 45
    },
    // search
    searchTop: {
        width,
        height: 195,
        zIndex: 5
    },
    filter: {
        backgroundColor: "#000"
    },
    filterImg: {
        width,
        height: 150,
        opacity: 0.7
    },
    filterTitle: {
        position: "absolute",
        width: 150,
        left: width / 2,
        marginLeft: -75,
        fontSize: 20,
        color: "#fff",
        textAlign: "center",
        backgroundColor: "transparent"
    },
    searchInputContainer: {
        height: 45,
        backgroundColor: "#fff",
        justifyContent: "center"
    },
    searchInput: {
        height: 45,
        fontSize: 18,
        color: "#4a575b",
        paddingHorizontal: 10
    },
    searchBtn: {
        position: "absolute",
        bottom: -40,
        left: 0,
        right: 0,
        height: 40,
        backgroundColor: "#db4234"
    },
    removeBtn: {
        height: 40,
        backgroundColor: "#db4234"
    }
});
