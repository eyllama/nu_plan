import React from 'react';
import { DrawerNavigator, StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import { Icon } from 'react-native-elements';
import Schedule from './screens/tabs/Schedule';
import Search from './screens/tabs/Search';
import CourseDetails from './screens/CourseDetails';
import SectionDetails from './screens/SectionDetails';

const Tabs = TabNavigator({
    Schedule: {
        screen: Schedule,
        navigationOptions: ({ navigation }) => ({
            // tabBarVisible: false
            tabBarIcon: ({ tintColor }) =>
                <Icon type="materialicons" name="view-agenda" color={tintColor} size={20} />
        })
    },
    Search: {
        screen: Search,
        navigationOptions: ({ navigation }) => ({
            // tabBarVisible: false
            tabBarIcon: ({ tintColor }) =>
                <Icon type="feather" name="search" color={tintColor} size={20} />
        })
    }
}, {
    tabBarPosition: "bottom",
    tabBarComponent: TabBarBottom,
    tabBarOptions: {
        style: {
            height: 45,
            backgroundColor: "#fff",
            borderTopWidth: 1,
            borderTopColor: "#efefef"
        },
        activeTintColor: "#4a575b",
        inactiveTintColor: "#efefef",
        showLabel: false
    },
    animationEnabled: true,
    swipeEnabled: true
});

export const Root = StackNavigator({
    Tabs: {
        screen: Tabs,
        navigationOptions: ({ navigation }) => ({

        })
    },
    Details: {
        screen: CourseDetails,
        navigationOptions: ({ navigation }) => ({

        })
    },
    SectionDetails: {
        screen: SectionDetails,
        navigationOptions: ({ navigation }) => ({

        })
    }
}, {
    headerMode: "none",
    initialRouteName: "Tabs",
    mode: "modal",
    cardStyle: {
        shadowOpacity: 0,
        shadowRadius: 0
    }
});

// export const Root = DrawerNavigator({
//     Main: {
//         screen: Nav,
//         navigationOptions: ({ navigation }) => ({
//
//         })
//     }
// }, {
//     initialRoute: "Main",
//     drawerOpenRoute: 'DrawerOpen',
//     drawerCloseRoute: 'DrawerClose',
//     drawerToggleRoute: 'DrawerToggle',
//     drawerBackgroundColor: "#4a575b",
//     drawerWidth: 100
// });
